<?php
/**
 * @file
 * Wrap Word DOMWordsIterator library.
 */

/**
 * Iterates individual words of DOM text and CDATA nodes.
 *
 * Also keeps track of the word position in the document.
 *
 * Based on the demonstration article by Patrick Galbraith
 *
 * @see http://www.pjgalbraith.com/truncating-text-html-with-php/
 *
 * Example:
 *
 *  $doc = new DOMDocument();
 *  $doc->load('example.xml');
 *  foreach(new WrapWordDOMWordsIterator($doc) as $word) echo $word;
 */
final class WrapWordDOMWordsIterator implements Iterator {

  private $start;
  private $current;
  private $offset;
  private $key;
  private $words;

  /**
   * Construct an instance - See DOMDocument::load and DOMDocument::loadHTML.
   *
   * @param DOMNode $el
   *   Expects DOMNode as DOMElement or DOMDocument.
   */
  public function __construct(DOMNode $el) {
    if ($el instanceof DOMDocument) {
      $this->start = $el->documentElement;
    }
    else {
      if ($el instanceof DOMElement) {
        $this->start = $el;
      }
      else {
        throw new InvalidArgumentException("Invalid arguments, expected DOMElement or DOMDocument");
      }
    }
  }

  /**
   * Returns position in text as DOMText node and character offset.
   *
   * @return array
   *   It's NOT a byte offset, you must use mb_substr() or similar to use this
   *   offset properly) node may be NULL if iterator has finished.
   */
  public function currentWordPosition() {
    return array($this->current, $this->offset, $this->words);
  }

  /**
   * Returns the current DOMElement.
   *
   * @return DOMElement
   *   The DOMElement that is currently being iterated or NULL if iterator has
   *   finished.
   */
  public function currentElement() {
    return $this->current ? $this->current->parentNode : NULL;
  }

  /**
   * Implements key() (Iterator interface).
   */
  public function key() {
    return $this->key;
  }

  /**
   * Implements next() (Iterator interface).
   */
  public function next() {
    if (!$this->current) {
      return;
    }

    if ($this->current->nodeType == XML_TEXT_NODE || $this->current->nodeType == XML_CDATA_SECTION_NODE) {
      if ($this->offset == -1) {
        // Fastest way to get individual Unicode chars and does not require
        // mb_* functions.
        // preg_match_all('/./us',$this->current->textContent,$m);
        // $this->words = $m[0];
        $this->words = preg_split("/[\n\r\t ]+/", $this->current->textContent, -1, PREG_SPLIT_NO_EMPTY | PREG_SPLIT_OFFSET_CAPTURE);
      }
      $this->offset++;

      if ($this->offset < count($this->words)) {
        $this->key++;
        return;
      }
      $this->offset = -1;
    }

    while ($this->current->nodeType == XML_ELEMENT_NODE && $this->current->firstChild) {
      $this->current = $this->current->firstChild;
      if ($this->current->nodeType == XML_TEXT_NODE || $this->current->nodeType == XML_CDATA_SECTION_NODE) {
        return $this->next();
      }
    }

    while (!$this->current->nextSibling && $this->current->parentNode) {
      $this->current = $this->current->parentNode;
      if ($this->current === $this->start) {
        $this->current = NULL;
        return;
      }
    }

    $this->current = $this->current->nextSibling;

    return $this->next();
  }

  /**
   * Implements current() (Iterator interface).
   */
  public function current() {
    if ($this->current) {
      return $this->words[$this->offset][0];
    }
    return NULL;
  }

  /**
   * Implements valid() (Iterator interface).
   */
  public function valid() {
    return !!$this->current;
  }

  /**
   * Implements rewind() (Iterator interface).
   */
  public function rewind() {
    $this->offset = -1;
    $this->words = array();
    $this->current = $this->start;
    $this->next();
  }

}
