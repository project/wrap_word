<?php
/**
 * @file
 * Wrap Word helpers.
 */

/**
 * Splits a DOMText node into multiple DOMText nodes, around a word.
 *
 * The DOMDocument is altered by reference.
 *
 * @param DOMText $text
 *   The DOMText element to split. In all cases, this node will be removed from
 *   the DOM after calling this function and replaced with up to three new ones.
 * @param int $offset
 *   The start position of the word we want to split around.
 * @param int $length
 *   The length of the word we want to split around. Can be zero.
 *
 * @examples
 *   If $dom_text has the value "Some example text" in the DOM as follows:
 *
 *      PARENT
 *        - DOMText: "Some example text"
 *
 *   Then each of the following function calls modifies the DOM thus:
 *
 * @example
 *     wrap_word_split_dom_text_node_around_substr($dom_text, 0, 4);
 *
 *       PARENT
 *         - DOMText: "Some"
 *         - DOMText: " example text"
 *
 * @example
 *     wrap_word_split_dom_text_node_around_substr($dom_text, 5, 7);
 *
 *       PARENT
 *         - DOMText: "Some "
 *         - DOMText: "example"
 *         - DOMText: " text"
 *
 * @example
 *     wrap_word_split_dom_text_node_around_substr($dom_text, 13, 4);
 *
 *       PARENT
 *         - DOMText: "Some example "
 *         - DOMText: "text"
 *
 * @example
 *     wrap_word_split_dom_text_node_around_substr($dom_text, 9, 0);
 *
 *       PARENT
 *         - DOMText: "Some exam"
 *         - DOMText: "ple text"
 *
 * @return DOMNode
 *   The new DOMText node representing the chosen word,
 *   OR, if $length was 0, the parent DOMNode instead.
 */
function wrap_word_split_dom_text_node_around_substr(DOMText $text, $offset, $length) {
  $dom = $text->ownerDocument;
  $return = $text->parentNode;
  if ($offset !== 0) {
    $before = $dom->createTextNode(drupal_substr($text->nodeValue, 0, $offset));
    $text->parentNode->insertBefore($before, $text);
  }
  if ($length > 0) {
    $during = $dom->createTextNode(drupal_substr($text->nodeValue, $offset, $length));
    $text->parentNode->insertBefore($during, $text);
    $return = $during;
  }
  $end_offset = $offset + $length;
  if ($end_offset < drupal_strlen($text->nodeValue)) {
    $after = $dom->createTextNode(drupal_substr($text->nodeValue, $end_offset));
    $text->parentNode->insertBefore($after, $text);
  }
  $text->parentNode->removeChild($text);
  return $return;
}

/**
 * Wraps the given DOMElement in a new HTML Tag.
 *
 * The document itself is modified by reference.
 *
 * @param DOMNode $el
 *   The element to wrap.
 * @param string $tag_name
 *   The name of the HTML tag to create as a wrapper.
 * @param array $attributes
 *   Standard Drupal 7 attributes array.
 *
 * @return bool
 *   TRUE if successful (and the DOM was modified),
 *   FALSE if failed. Typically this will be because $tag_name contains invalid
 *   characters for an HTML tag name.
 */
function wrap_word_wrap_dom_element(DOMNode $el, $tag_name, $attributes = array()) {
  $doc = $el->ownerDocument;
  if ($wrapper_el = $doc->createElement($tag_name)) {
    foreach ($attributes as $name => $value) {
      if (is_array($value)) {
        $value = implode(' ', $value);
      }
      $wrapper_el->setAttribute($name, $value);
    }
    $el->parentNode->replaceChild($wrapper_el, $el);
    $wrapper_el->appendChild($el);
    return TRUE;
  }
  return FALSE;
}
