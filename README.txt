 CONTENTS OF THIS FILE
 ---------------------
  * Introduction
  * Installation
  * Configuration
  * Unit Testing
  * Maintainers


 INTRODUCTION
 ------------
 The Wrap Word module allows a user to create a text format that wraps a
 word (determined by position) in HTML tags.

  * For a full description of the module, visit the project page:
    https://drupal.org/project/wrap_word

  * To submit bug reports and feature suggestions, or to track changes:
    https://drupal.org/project/issues/wrap_word


 REQUIREMENTS
 ------------
 No special requirements.


 INSTALLATION
 ------------
  * Install as you would normally install a contributed Drupal module. See:
    https://drupal.org/documentation/install/modules-themes/modules-7
    for further information.


 CONFIGURATION
 -------------
 The module has no menu or modifiable settings. There is no configuration.

 When enabled, the module will show a text format option entitled "Wrap Word".


 UNIT TESTING
 ------------
 This module uses PHPUnit as the foundation for unit testing. All unit tests
 must be placed under wrap_word/tests.

 At present, there is one test file "WrapWordMainTest.php" which tests the core
 functionality of the text format. Please note, the test is only aware of
 div, p, span tags.

 IMPORTANT:
 - All tests must always pass!

 USAGE: Run the following command from within the wrap_word/ folder: phpunit


 MAINTAINERS
 -----------
 Current maintainers:
  * HenryW - https://www.drupal.org/u/henryw

 This project has been supported by:
  * Capgemini - https://www.drupal.org/node/1772260
