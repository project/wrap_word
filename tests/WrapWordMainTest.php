<?php
/**
 * @file
 * Wrap Word main unit test.
 */

/**
 * Tests functionality of the Wrap Word text format.
 */
class WrapWordMainTest extends PHPUnit_Framework_TestCase {
  /**
   * Send the provided data through the Wrap Word text format process.
   *
   * @dataProvider data
   */
  public function testFormat($input, $settings, $output) {
    $this->assertEquals(_wrap_word_process($input, $settings), $output);
  }

  /**
   * Data for testing.
   *
   * @return array
   *   The data to be tested.
   */
  public function data() {
    $inputs = array(
      // 0
      'This is a test',
      // 1
      'Test',
      // 2
      '',
      // 3
      ' ',
      // 4
      '<strong>This</strong> is a test',
      // 5
      '<strong>This WORD</strong> is a test',
    );

    $positions = array(
      // 0
      'first',
      // 1
      'last',
    );

    $tags = array(
      // 0
      array('div', 'p', 'span'),
      // 1
      array('div', 'p'),
      // 2
      array('div', 'span'),
      // 3
      array('p', 'span'),
      // 4
      array('div'),
      // 5
      array('p'),
      // 6
      array('span'),
    );

    $classes = array(
      // 0
      '',
      // 1
      'test',
      // 2
      'test testing',
      // 3
      ' ',
      // 4
      '.',
      // 5
      'my_test_class',
    );

    $expectances = array(
      // FIRST WORD
      // 0
      '<div><p><span>This</span></p></div> is a test',
      // 1
      '<div><p>This</p></div> is a test',
      // 2
      '<div><span>This</span></div> is a test',
      // 3
      '<p><span>This</span></p> is a test',
      // 4
      '<div>This</div> is a test',
      // 5
      '<p>This</p> is a test',
      // 6
      '<span>This</span> is a test',
      // LAST WORD
      // 7
      'This is a <div><p><span>test</span></p></div>',
      // 8
      'This is a <div><p>test</p></div>',
      // 9
      'This is a <div><span>test</span></div>',
      // 10
      'This is a <p><span>test</span></p>',
      // 11
      'This is a <div>test</div>',
      // 12
      'This is a <p>test</p>',
      // 13
      'This is a <span>test</span>',
      // SINGLE WORD
      // 14
      '<div><p><span>Test</span></p></div>',
      // 15
      '<div><p>Test</p></div>',
      // 16
      '<div><span>Test</span></div>',
      // 17
      '<p><span>Test</span></p>',
      // 18
      '<div>Test</div>',
      // 19
      '<p>Test</p>',
      // 20
      '<span>Test</span>',
      // EMPTY AND WHITESPACE
      // 21
      '',
      // 22
      ' ',
      // TAG IN INPUT
      // 23
      '<strong><div><p><span>This</span></p></div></strong> is a test',
      // 24
      '<strong><div><p>This</p></div></strong> is a test',
      // 25
      '<strong><div><span>This</span></div></strong> is a test',
      // 26
      '<strong><p><span>This</span></p></strong> is a test',
      // 27
      '<strong><div>This</div></strong> is a test',
      // 28
      '<strong><p>This</p></strong> is a test',
      // 29
      '<strong><span>This</span></strong> is a test',
      // TAG IN INPUT WITH MORE THAN ONE WORD
      // 30
      '<strong><div><p><span>This</span></p></div> WORD</strong> is a test',
      // 31
      '<strong><div><p>This</p></div> WORD</strong> is a test',
      // 32
      '<strong><div><span>This</span></div> WORD</strong> is a test',
      // 33
      '<strong><p><span>This</span></p> WORD</strong> is a test',
      // 34
      '<strong><div>This</div> WORD</strong> is a test',
      // 35
      '<strong><p>This</p> WORD</strong> is a test',
      // 36
      '<strong><span>This</span> WORD</strong> is a test',
      // EMPTY CLASS ON ALL TAGS
      // 37
      '<div><p><span>This</span></p></div> is a test',
      // ONE CLASS ON DIV
      // 38
      '<div class="test"><p><span>This</span></p></div> is a test',
      // 39
      '<div class="test"><p>This</p></div> is a test',
      // 40
      '<div class="test"><span>This</span></div> is a test',
      // 41
      '<div class="test">This</div> is a test',
      // ONE CLASS ON P
      // 42
      '<div><p class="test"><span>This</span></p></div> is a test',
      // 43
      '<div><p class="test">This</p></div> is a test',
      // 44
      '<p class="test"><span>This</span></p> is a test',
      // 45
      '<p class="test">This</p> is a test',
      // ONE CLASS ON SPAN
      // 46
      '<div><p><span class="test">This</span></p></div> is a test',
      // 47
      '<div><span class="test">This</span></div> is a test',
      // 48
      '<p><span class="test">This</span></p> is a test',
      // 49
      '<span class="test">This</span> is a test',
      // MULTIPLE CLASSES ON DIV
      // 50
      '<div class="test testing"><p><span>This</span></p></div> is a test',
      // 51
      '<div class="test testing"><p>This</p></div> is a test',
      // 52
      '<div class="test testing"><span>This</span></div> is a test',
      // 53
      '<div class="test testing">This</div> is a test',
      // MULTIPLE CLASSES ON P
      // 54
      '<div><p class="test testing"><span>This</span></p></div> is a test',
      // 55
      '<div><p class="test testing">This</p></div> is a test',
      // 56
      '<p class="test testing"><span>This</span></p> is a test',
      // 57
      '<p class="test testing">This</p> is a test',
      // MULTIPLE CLASSES ON SPAN
      // 58
      '<div><p><span class="test testing">This</span></p></div> is a test',
      // 59
      '<div><span class="test testing">This</span></div> is a test',
      // 60
      '<p><span class="test testing">This</span></p> is a test',
      // 61
      '<span class="test testing">This</span> is a test',
      // INVALID CLASS NAMES
      // 62 & 63
      '<span>This</span> is a test',
      // 64
      '<span class="my-test-class">This</span> is a test',
    );

    $combinations = array(
      //
      // TEST 0 (All tags on first word)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p', 'span')'
      // EXPECTED: '<div><p><span>This</span></p></div> is a test'
      //
      array($inputs[0], $positions[0], $tags[0], $expectances[0]),
      //
      // TEST 1 (Div and P tags on first word)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p')'
      // EXPECTED: '<div><p>This</p></div> is a test'
      //
      array($inputs[0], $positions[0], $tags[1], $expectances[1]),
      //
      // TEST 2 (Div and Span tags on first word)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'span')'
      // EXPECTED: '<div><span>This</span></div> is a test'
      //
      array($inputs[0], $positions[0], $tags[2], $expectances[2]),
      //
      // TEST 3 (P and Span tags on first word)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('p', 'span')'
      // EXPECTED: '<p><span>This</span></p> is a test'
      //
      array($inputs[0], $positions[0], $tags[3], $expectances[3]),
      //
      // TEST 4 (Div tag on first word)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div')'
      // EXPECTED: '<div>This</div> is a test'
      //
      array($inputs[0], $positions[0], $tags[4], $expectances[4]),
      //
      // TEST 5 (P tag on first word)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('p')'
      // EXPECTED: '<p>This</p> is a test'
      //
      array($inputs[0], $positions[0], $tags[5], $expectances[5]),
      //
      // TEST 6 (Span tag on first word)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('span')'
      // EXPECTED: '<span>This</span> is a test'
      //
      array($inputs[0], $positions[0], $tags[6], $expectances[6]),
      //
      // TEST 7 (All tags on last word)
      //
      // INPUT: 'This is a test'
      // POSITION: 'last'
      // TAGS: 'array('div', 'p', 'span')'
      // EXPECTED: 'This is a <div><p><span>test</span></p></div>'
      //
      array($inputs[0], $positions[1], $tags[0], $expectances[7]),
      //
      // TEST 8 (Div and P tags on last word)
      //
      // INPUT: 'This is a test'
      // POSITION: 'last'
      // TAGS: 'array('div', 'p')'
      // EXPECTED: 'This is a <div><p>test</p></div>'
      //
      array($inputs[0], $positions[1], $tags[1], $expectances[8]),
      //
      // TEST 9 (Div and Span tags on last word)
      //
      // INPUT: 'This is a test'
      // POSITION: 'last'
      // TAGS: 'array('div', 'span')'
      // EXPECTED: 'This is a <div><span>test</span></div>'
      //
      array($inputs[0], $positions[1], $tags[2], $expectances[9]),
      //
      // TEST 10 (P and Span tags on last word)
      //
      // INPUT: 'This is a test'
      // POSITION: 'last'
      // TAGS: 'array('p', 'span')'
      // EXPECTED: 'This is a <p><span>test</span></p>'
      //
      array($inputs[0], $positions[1], $tags[3], $expectances[10]),
      //
      // TEST 11 (Div tag on last word)
      //
      // INPUT: 'This is a test'
      // POSITION: 'last'
      // TAGS: 'array('div')'
      // EXPECTED: 'This is a <div>test</div>'
      //
      array($inputs[0], $positions[1], $tags[4], $expectances[11]),
      //
      // TEST 12 (P tag on last word)
      //
      // INPUT: 'This is a test'
      // POSITION: 'last'
      // TAGS: 'array('p')'
      // EXPECTED: 'This is a <p>test</p>'
      //
      array($inputs[0], $positions[1], $tags[5], $expectances[12]),
      //
      // TEST 13 (Span tag on last word)
      //
      // INPUT: 'This is a test'
      // POSITION: 'last'
      // TAGS: 'array('span')'
      // EXPECTED: 'This is a <span>test</span>'
      //
      array($inputs[0], $positions[1], $tags[6], $expectances[13]),
      //
      // TEST 14 (All tags on single word)
      //
      // INPUT: 'Test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p', 'span')'
      // EXPECTED: '<div><p><span>Test</span></p></div>'
      //
      array($inputs[1], $positions[0], $tags[0], $expectances[14]),
      //
      // TEST 15 (Div and P tags on single word)
      //
      // INPUT: 'Test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p')'
      // EXPECTED: '<div><p>Test</p></div>'
      //
      array($inputs[1], $positions[0], $tags[1], $expectances[15]),
      //
      // TEST 16 (Div and Span tags on single word)
      //
      // INPUT: 'Test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'span')'
      // EXPECTED: '<div><span>Test</span></div>'
      //
      array($inputs[1], $positions[0], $tags[2], $expectances[16]),
      //
      // TEST 17 (P and Span tags on single word)
      //
      // INPUT: 'Test'
      // POSITION: 'first'
      // TAGS: 'array('p', 'span')'
      // EXPECTED: '<p><span>Test</span></p>'
      //
      array($inputs[1], $positions[0], $tags[3], $expectances[17]),
      //
      // TEST 18 (Div tag on single word)
      //
      // INPUT: 'Test'
      // POSITION: 'first'
      // TAGS: 'array('div')'
      // EXPECTED: '<div>Test</div>'
      //
      array($inputs[1], $positions[0], $tags[4], $expectances[18]),
      //
      // TEST 19 (P tag on single word)
      //
      // INPUT: 'Test'
      // POSITION: 'first'
      // TAGS: 'array('p')'
      // EXPECTED: '<p>Test</p>'
      //
      array($inputs[1], $positions[0], $tags[5], $expectances[19]),
      //
      // TEST 20 (Span tag on single word)
      //
      // INPUT: 'Test'
      // POSITION: 'first'
      // TAGS: 'array('span')'
      // EXPECTED: '<span>Test</span>'
      //
      array($inputs[1], $positions[0], $tags[6], $expectances[20]),
      //
      // TEST 21 (Empty input)
      //
      // INPUT: ''
      // POSITION: 'first'
      // TAGS: 'array('span')'
      // EXPECTED: ''
      //
      array($inputs[2], $positions[0], $tags[6], $expectances[21]),
      //
      // TEST 22 (Single whitespace input)
      //
      // INPUT: ' '
      // POSITION: 'first'
      // TAGS: 'array('span')'
      // EXPECTED: ' '
      //
      array($inputs[3], $positions[0], $tags[6], $expectances[22]),
      //
      // TEST 23 (All tags on first word with tag already)
      //
      // INPUT: '<strong>This</strong> is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p', 'span')'
      // EXPECTED:
      // '<strong><div><p><span>This</span></p></div></strong> is a test'
      //
      array($inputs[4], $positions[0], $tags[0], $expectances[23]),
      //
      // TEST 24 (Div and P tags on first word with tag already)
      //
      // INPUT: '<strong>This</strong> is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p')'
      // EXPECTED: '<strong><div><p>This</p></div></strong> is a test'
      //
      array($inputs[4], $positions[0], $tags[1], $expectances[24]),
      //
      // TEST 25 (Div and Span tags on first word with tag already)
      //
      // INPUT: '<strong>This</strong> is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'span')'
      // EXPECTED: '<strong><div><span>This</span></div></strong> is a test'
      //
      array($inputs[4], $positions[0], $tags[2], $expectances[25]),
      //
      // TEST 26 (P and Span tags on first word with tag already)
      //
      // INPUT: '<strong>This</strong> is a test'
      // POSITION: 'first'
      // TAGS: 'array('p', 'span')'
      // EXPECTED: '<strong><p><span>This</span></p></strong> is a test'
      //
      array($inputs[4], $positions[0], $tags[3], $expectances[26]),
      //
      // TEST 27 (Div tag on first word with tag already)
      //
      // INPUT: '<strong>This</strong> is a test'
      // POSITION: 'first'
      // TAGS: 'array('div')'
      // EXPECTED: '<strong><div>This</div></strong> is a test'
      //
      array($inputs[4], $positions[0], $tags[4], $expectances[27]),
      //
      // TEST 28 (P tag on first word with tag already)
      //
      // INPUT: '<strong>This</strong> is a test'
      // POSITION: 'first'
      // TAGS: 'array('p')'
      // EXPECTED: '<strong><p>This</p></strong> is a test'
      //
      array($inputs[4], $positions[0], $tags[5], $expectances[28]),
      //
      // TEST 29 (Span tag on first word with tag already)
      //
      // INPUT: '<strong>This</strong> is a test'
      // POSITION: 'first'
      // TAGS: 'array('span')'
      // EXPECTED: '<strong><span>This</span></strong> is a test'
      //
      array($inputs[4], $positions[0], $tags[6], $expectances[29]),
      //
      // TEST 30 (All tags on first word out of multiple with tag already)
      //
      // INPUT: '<strong>This WORD</strong> is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p', 'span')'
      // EXPECTED:
      // '<strong><div><p><span>This</span></p></div> WORD</strong> is a test'
      //
      array($inputs[5], $positions[0], $tags[0], $expectances[30]),
      //
      // TEST 31 (Div and P tags on first word out of multiple with tag already)
      //
      // INPUT: '<strong>This WORD</strong> is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p')'
      // EXPECTED: '<strong><div><p>This</p></div> WORD</strong> is a test'
      //
      array($inputs[5], $positions[0], $tags[1], $expectances[31]),
      //
      // TEST 32
      // (Div and Span tags on first word out of multiple with tag already)
      //
      // INPUT: '<strong>This WORD</strong> is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'span')'
      // EXPECTED:
      // '<strong><div><span>This</span></div> WORD</strong> is a test'
      //
      array($inputs[5], $positions[0], $tags[2], $expectances[32]),
      //
      // TEST 33
      // (P and Span tags on first word out of multiple with tag already)
      //
      // INPUT: '<strong>This WORD</strong> is a test'
      // POSITION: 'first'
      // TAGS: 'array('p', 'span')'
      // EXPECTED: '<strong><p><span>This</span></p> WORD</strong> is a test'
      //
      array($inputs[5], $positions[0], $tags[3], $expectances[33]),
      //
      // TEST 34 (Div tag on first word out of multiple with tag already)
      //
      // INPUT: '<strong>This WORD</strong> is a test'
      // POSITION: 'first'
      // TAGS: 'array('div')'
      // EXPECTED: '<strong><div>This</div> WORD</strong> is a test'
      //
      array($inputs[5], $positions[0], $tags[4], $expectances[34]),
      //
      // TEST 35 (P tag on first word out of multiple with tag already)
      //
      // INPUT: '<strong>This WORD</strong> is a test'
      // POSITION: 'first'
      // TAGS: 'array('p')'
      // EXPECTED: '<strong><p>This</p> WORD</strong> is a test'
      //
      array($inputs[5], $positions[0], $tags[5], $expectances[35]),
      //
      // TEST 36 (Span tag on first word out of multiple with tag already)
      //
      // INPUT: '<strong>This WORD</strong> is a test'
      // POSITION: 'first'
      // TAGS: 'array('span')'
      // EXPECTED: '<strong><span>This</span> WORD</strong> is a test'
      //
      array($inputs[5], $positions[0], $tags[6], $expectances[36]),
      //
      // TEST 37 (Empty class on all tags)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p', 'span')'
      // DIV CLASS: ''
      // P CLASS: ''
      // SPAN CLASS: ''
      // EXPECTED: '<div><p><span>This</span></p></div> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[0],
        $expectances[37],
        $classes[0],
        $classes[0],
        $classes[0],
      ),
      //
      // TEST 38 (One class on div tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p', 'span')'
      // DIV CLASS: 'test'
      // P CLASS: ''
      // SPAN CLASS: ''
      // EXPECTED: '<div class="test"><p><span>This</span></p></div> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[0],
        $expectances[38],
        $classes[1],
        $classes[0],
        $classes[0],
      ),
      //
      // TEST 39 (One class on div tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p')'
      // DIV CLASS: 'test'
      // P CLASS: ''
      // SPAN CLASS: ''
      // EXPECTED: '<div class="test"><p>This</p></div> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[1],
        $expectances[39],
        $classes[1],
        $classes[0],
        $classes[0],
      ),
      //
      // TEST 40 (One class on div tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'span')'
      // DIV CLASS: 'test'
      // P CLASS: ''
      // SPAN CLASS: ''
      // EXPECTED: '<div class="test"><span>This</span></div> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[2],
        $expectances[40],
        $classes[1],
        $classes[0],
        $classes[0],
      ),
      //
      // TEST 41 (One class on div tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div')'
      // DIV CLASS: 'test'
      // P CLASS: ''
      // SPAN CLASS: ''
      // EXPECTED: '<div class="test">This</div> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[4],
        $expectances[41],
        $classes[1],
        $classes[0],
        $classes[0],
      ),
      //
      // TEST 42 (One class on p tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p', 'span')'
      // DIV CLASS: ''
      // P CLASS: 'test'
      // SPAN CLASS: ''
      // EXPECTED: '<div><p class="test"><span>This</span></p></div> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[0],
        $expectances[42],
        $classes[0],
        $classes[1],
        $classes[0],
      ),
      //
      // TEST 43 (One class on p tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p')'
      // DIV CLASS: ''
      // P CLASS: 'test'
      // SPAN CLASS: ''
      // EXPECTED: '<div><p class="test">This</p></div> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[1],
        $expectances[43],
        $classes[0],
        $classes[1],
        $classes[0],
      ),
      //
      // TEST 44 (One class on p tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('p', 'span')'
      // DIV CLASS: ''
      // P CLASS: 'test'
      // SPAN CLASS: ''
      // EXPECTED: '<p class="test"><span>This</span></p> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[3],
        $expectances[44],
        $classes[0],
        $classes[1],
        $classes[0],
      ),
      //
      // TEST 45 (One class on p tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('p')'
      // DIV CLASS: ''
      // P CLASS: 'test'
      // SPAN CLASS: ''
      // EXPECTED: '<p class="test">This</p> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[5],
        $expectances[45],
        $classes[0],
        $classes[1],
        $classes[0],
      ),
      //
      // TEST 46 (One class on span tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p', 'span')'
      // DIV CLASS: ''
      // P CLASS: ''
      // SPAN CLASS: 'test'
      // EXPECTED: '<div><p><span class="test">This</span></p></div> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[0],
        $expectances[46],
        $classes[0],
        $classes[0],
        $classes[1],
      ),
      //
      // TEST 47 (One class on span tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'span')'
      // DIV CLASS: ''
      // P CLASS: ''
      // SPAN CLASS: 'test'
      // EXPECTED: '<div><span class="test">This</p></div> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[2],
        $expectances[47],
        $classes[0],
        $classes[0],
        $classes[1],
      ),
      //
      // TEST 48 (One class on span tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('p', 'span')'
      // DIV CLASS: ''
      // P CLASS: ''
      // SPAN CLASS: 'test'
      // EXPECTED: '<p><span class="test">This</span></p> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[3],
        $expectances[48],
        $classes[0],
        $classes[0],
        $classes[1],
      ),
      //
      // TEST 49 (One class on span tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('span')'
      // DIV CLASS: ''
      // P CLASS: ''
      // SPAN CLASS: 'test'
      // EXPECTED: '<span class="test">This</span> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[6],
        $expectances[49],
        $classes[0],
        $classes[0],
        $classes[1],
      ),
      //
      // TEST 50 (Multiple classes on div tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p', 'span')'
      // DIV CLASS: 'test testing'
      // P CLASS: ''
      // SPAN CLASS: ''
      // EXPECTED:
      // '<div class="test testing"><p><span>This</span></p></div> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[0],
        $expectances[50],
        $classes[2],
        $classes[0],
        $classes[0],
      ),
      //
      // TEST 51 (Multiple classes on div tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p')'
      // DIV CLASS: 'test testing'
      // P CLASS: ''
      // SPAN CLASS: ''
      // EXPECTED: '<div class="test testing"><p>This</p></div> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[1],
        $expectances[51],
        $classes[2],
        $classes[0],
        $classes[0],
      ),
      //
      // TEST 52 (Multiple classes on div tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'span')'
      // DIV CLASS: 'test testing'
      // P CLASS: ''
      // SPAN CLASS: ''
      // EXPECTED: '<div class="test testing"><span>This</span></div> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[2],
        $expectances[52],
        $classes[2],
        $classes[0],
        $classes[0],
      ),
      //
      // TEST 53 (Multiple classes on div tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div')'
      // DIV CLASS: 'test testing'
      // P CLASS: ''
      // SPAN CLASS: ''
      // EXPECTED: '<div class="test testing">This</div> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[4],
        $expectances[53],
        $classes[2],
        $classes[0],
        $classes[0],
      ),
      //
      // TEST 54 (Multiple classes on p tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p', 'span')'
      // DIV CLASS: ''
      // P CLASS: 'test testing'
      // SPAN CLASS: ''
      // EXPECTED:
      // '<div><p class="test testing"><span>This</span></p></div> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[0],
        $expectances[54],
        $classes[0],
        $classes[2],
        $classes[0],
      ),
      //
      // TEST 55 (Multiple classes on p tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p')'
      // DIV CLASS: ''
      // P CLASS: 'test testing'
      // SPAN CLASS: ''
      // EXPECTED: '<div><p class="test testing">This</p></div> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[1],
        $expectances[55],
        $classes[0],
        $classes[2],
        $classes[0],
      ),
      //
      // TEST 56 (Multiple classes on p tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('p', 'span')'
      // DIV CLASS: ''
      // P CLASS: 'test testing'
      // SPAN CLASS: ''
      // EXPECTED: '<p class="test testing"><span>This</span></p> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[3],
        $expectances[56],
        $classes[0],
        $classes[2],
        $classes[0],
      ),
      //
      // TEST 57 (Multiple classes on p tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('p')'
      // DIV CLASS: ''
      // P CLASS: 'test testing'
      // SPAN CLASS: ''
      // EXPECTED: '<p class="test testing">This</p> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[5],
        $expectances[57],
        $classes[0],
        $classes[2],
        $classes[0],
      ),
      //
      // TEST 58 (Multiple classes on span tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'p', 'span')'
      // DIV CLASS: ''
      // P CLASS: ''
      // SPAN CLASS: 'test testing'
      // EXPECTED:
      // '<div><p><span class="test testing">This</span></p></div> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[0],
        $expectances[58],
        $classes[0],
        $classes[0],
        $classes[2],
      ),
      //
      // TEST 59 (Multiple classes on span tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('div', 'span')'
      // DIV CLASS: ''
      // P CLASS: ''
      // SPAN CLASS: 'test testing'
      // EXPECTED: '<div><span class="test testing">This</p></div> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[2],
        $expectances[59],
        $classes[0],
        $classes[0],
        $classes[2],
      ),
      //
      // TEST 60 (Multiple classes on span tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('p', 'span')'
      // DIV CLASS: ''
      // P CLASS: ''
      // SPAN CLASS: 'test testing'
      // EXPECTED: '<p><span class="test testing">This</span></p> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[3],
        $expectances[60],
        $classes[0],
        $classes[0],
        $classes[2],
      ),
      //
      // TEST 61 (Multiple classes on span tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('span')'
      // DIV CLASS: ''
      // P CLASS: ''
      // SPAN CLASS: 'test testing'
      // EXPECTED: '<span class="test testing">This</span> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[6],
        $expectances[61],
        $classes[0],
        $classes[0],
        $classes[2],
      ),
      //
      // TEST 62 (Single whitespace as class name on tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('span')'
      // DIV CLASS: ''
      // P CLASS: ''
      // SPAN CLASS: ' '
      // EXPECTED: '<span>This</span> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[6],
        $expectances[62],
        $classes[0],
        $classes[0],
        $classes[3],
      ),
      //
      // TEST 63 (Special character (period) as class name on tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('span')'
      // DIV CLASS: ''
      // P CLASS: ''
      // SPAN CLASS: '.'
      // EXPECTED: '<span>This</span> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[6],
        $expectances[62],
        $classes[0],
        $classes[0],
        $classes[4],
      ),
      //
      // TEST 65 (Underscores converted to hyphens as class name on tag)
      //
      // INPUT: 'This is a test'
      // POSITION: 'first'
      // TAGS: 'array('span')'
      // DIV CLASS: ''
      // P CLASS: ''
      // SPAN CLASS: 'my_test_class'
      // EXPECTED: '<span class="my-test-class">This</span> is a test'
      //
      array(
        $inputs[0],
        $positions[0],
        $tags[6],
        $expectances[63],
        $classes[0],
        $classes[0],
        $classes[5],
      ),
    );

    $return_array = array();

    foreach ($combinations as $combination) {
      $input = $combination[0];

      $settings_array = new stdClass();

      $settings_array->settings['position'] = $combination[1];
      $settings_array->settings['tags'] = $combination[2];
      $expected = $combination[3];
      $settings_array->settings['div_class'] = isset($combination[4]) ? $combination[4] : '';
      $settings_array->settings['p_class'] = isset($combination[5]) ? $combination[5] : '';
      $settings_array->settings['span_class'] = isset($combination[6]) ? $combination[6] : '';

      array_push(
        $return_array,
        array(
          $input,
          $settings_array,
          $expected,
        )
      );
    }

    return $return_array;
  }

}
