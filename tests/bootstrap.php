<?php
/**
 * @file
 * Wrap Word unit test bootstrap.
 */

// Ensure drupal is bootstrapped.
if (!defined('DRUPAL_ROOT')) {
  define('DRUPAL_ROOT', _wrap_word_filesystem_base_path());
}

/**
 * Find the drupal root based on where this file exists.
 */
function _wrap_word_filesystem_base_path() {
  if (!isset($GLOBALS['filesystem_base_path'])) {
    $search = "includes" . DIRECTORY_SEPARATOR . "bootstrap.inc";

    // Walk directories up until we find the $search-path (which is relative to root).
    for ($path = dirname(__FILE__); !file_exists($path . DIRECTORY_SEPARATOR . $search); $path .= DIRECTORY_SEPARATOR . "..") {
      // No need to do anything.
    }
    // Store the path if one was found.
    $GLOBALS['filesystem_base_path'] = realpath($path);
  }
  return $GLOBALS['filesystem_base_path'];
}

// Include the drupal bootstrap class.
require_once DRUPAL_ROOT . '/includes/bootstrap.inc';

// Set an unset REMOTE_ADDR to please bootstrap.inc.
if (!isset($_SERVER['REMOTE_ADDR'])) {
  $_SERVER['REMOTE_ADDR'] = '127.0.0.1';
}

// Include the whole of drupal.
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

// Include WrapWord module.
require_once __DIR__ . '/../wrap_word.module';

// Include WrapWordDOMWordsIterator class.
require_once __DIR__ . '/../libs/WrapWordDOMWordsIterator.inc';

// Include wrap_word_split_dom_text_node_around_substr().
require_once __DIR__ . '/../includes/wrap_word.helpers.inc';
